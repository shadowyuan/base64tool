/*
 *
 * Copyright (c) 2020 The Raptor Authors. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef RAPTOR_LITE_UTILS_LOG_H_
#define RAPTOR_LITE_UTILS_LOG_H_

#include <stdlib.h>
#include <string.h>

namespace raptor {
enum class LogLevel : int { kDebug, kInfo, kWarn, kError };

typedef struct {
    const char *file;
    int line;
    LogLevel level;
    const char *message;
    unsigned long threadid;
} LogArgument;

typedef void (*LogPrintCallback)(LogArgument *arg);

void LogSetLevel(LogLevel level);
void LogFormatPrint(const char *file, int line, LogLevel level, const char *format, ...);
void LogSetPrintCallback(LogPrintCallback callback);
} // namespace raptor

#ifdef _WIN32
#define __FILENAME__ (strrchr("\\" __FILE__, '\\') + 1)
#else
#ifdef __GNUC__
#define __FILENAME__ (__builtin_strrchr("/" __FILE__, '/') + 1)
#else
#define __FILENAME__ (strrchr("/" __FILE__, '/') + 1)
#endif // __GNUC__
#endif // _WIN32

#define log_debug(FMT, ...)                                                                        \
    raptor::LogFormatPrint(__FILENAME__, __LINE__, raptor::LogLevel::kDebug, FMT, ##__VA_ARGS__)

#define log_info(FMT, ...)                                                                         \
    raptor::LogFormatPrint(__FILENAME__, __LINE__, raptor::LogLevel::kInfo, FMT, ##__VA_ARGS__)

#define log_warn(FMT, ...)                                                                         \
    raptor::LogFormatPrint(__FILENAME__, __LINE__, raptor::LogLevel::kWarn, FMT, ##__VA_ARGS__)

#define log_error(FMT, ...)                                                                        \
    raptor::LogFormatPrint(__FILENAME__, __LINE__, raptor::LogLevel::kError, FMT, ##__VA_ARGS__)

#endif // RAPTOR_LITE_UTILS_LOG_H_
