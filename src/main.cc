#include <Windows.h>
#include "res/resource.h"
#include "src/view.h"

#if defined _M_IX86
#pragma comment(                                                                                   \
    linker,                                                                                        \
    "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(                                                                                   \
    linker,                                                                                        \
    "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(                                                                                   \
    linker,                                                                                        \
    "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

INT_PTR CALLBACK MainWindowProc(HWND hdlg, UINT message, WPARAM wparam, LPARAM lparam) {
    UNREFERENCED_PARAMETER(lparam);
    switch (message) {
    case WM_INITDIALOG:
        OnInitDlg(hdlg);
        break;
    case WM_COMMAND:
        OnCommand(hdlg, wparam);
        break;
    case WM_CLOSE:
        EndDialog(hdlg, LOWORD(wparam));
        break;
    default:
        break;
    }
    return 0;
}

int WINAPI wWinMain(HINSTANCE handle, HINSTANCE prev, LPWSTR cmd, int show) {
    UNREFERENCED_PARAMETER(prev);
    UNREFERENCED_PARAMETER(cmd);
    UNREFERENCED_PARAMETER(show);
    DialogBox(handle, MAKEINTRESOURCE(IDD_DIALOG1), NULL, (DLGPROC)MainWindowProc);
    return 0;
}
